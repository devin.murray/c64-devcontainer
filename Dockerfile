FROM archlinux:latest

ARG USER=c64
ARG HOME=/home/$USER
ARG USER_UID=1000
ARG USER_GID=$USER_UID

RUN pacman -Sy --noconfirm && pacman -S --noconfirm \
        acme \
        vice \
        git \
        curl \
        ninja \
        base-devel \
        clang \
        openssh \
    # Create a non-root user to use
    && useradd -m -G wheel $USER  

USER $USER