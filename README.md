# C64 Devcontainer

This is a development container for development on the commodore 64. It is intended to be an all in one package for commodore 64 development on mondern machines

## Features
1. [VS64](https://marketplace.visualstudio.com/items?itemName=rosc.vs64) - A VSCode extension for developing for the commodore 64, providing syntax highlighting, code completion, and debugging
2. [COMMODORE 64 BASIC V2](https://marketplace.visualstudio.com/items?itemName=gverduci.c64basicv2) - A VSCode extension for developing in BASIC for the commodore 64, providing keyboard shortcuts, and a menu for SID music
3. [VICE](https://vice-emu.sourceforge.io/) - A commodore 64 emulator for running, debugging, and exporting code
4. [ACME](https://sourceforge.net/projects/acme-crossass/) - A cross assembler for compiling 6502 assembly code

## Install

### Dependencies
- [VSCode Devcontainer Extension](https://marketplace.visualstudio.com/items?itemName=ms-vscode-remote.remote-containers)
- [Docker](https://docs.docker.com/engine/install/) or [Podman](https://podman.io/docs/installation)
- [WSL2](https://learn.microsoft.com/en-us/windows/wsl/install) (Windows only)

### Setup Project
You can add this development container to your project by cloning it into the root of your project

```bash
git clone https://gitlab.com/devin.murray/c64-devcontainer .devcontainer
```
Once cloned you can use the VScode Devcontainer extension to open the project in a container. This will build the container and open the project in a new window with all the tools you need to develop for the commodore 64.

